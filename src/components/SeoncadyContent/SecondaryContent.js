import styles from "./styles.module.scss";

const SecondaryContent = ({ content = [], title, handleClick }) => {
  if (content.length === 0) {
    return null;
  }
  return (
    <div className={styles.secondaryContent}>
      <h2>{title}</h2>
      <div className={styles.secondaryInnerContent}>
        <div className={styles.left}>
          {content[0].image ? (
            <img
              onClick={() => handleClick(content[0].url)}
              src={content[0].image}
            />
          ) : null}
          <h3 onClick={() => handleClick(content[0].url)}>
            {content[0].title}
          </h3>
          <p onClick={() => handleClick(content[0].url)}>
            {content[0].description}
          </p>
        </div>
        <div className={styles.right}>
          <div className={styles.rightHalf}>
            <div className={styles.card}>
              <div>
                <h3 onClick={() => handleClick(content[1].url)}>
                  {content[1].title}
                </h3>
                <p onClick={() => handleClick(content[1].url)}>
                  {content[1].description}
                </p>
              </div>
              {content[1].image ? (
                <img
                  onClick={() => handleClick(content[1].url)}
                  src={content[1].image}
                />
              ) : null}
            </div>
            <div className={styles.card}>
              <div>
                <h3 onClick={() => handleClick(content[2].url)}>
                  {content[2].title}
                </h3>
                <p onClick={() => handleClick(content[2].url)}>
                  {content[2].description}
                </p>
              </div>
              {content[2].image ? (
                <img
                  onClick={() => handleClick(content[2].url)}
                  src={content[2].image}
                />
              ) : null}
            </div>
            <div className={styles.card}>
              <div>
                <h3 onClick={() => handleClick(content[3].url)}>
                  {content[3].title}
                </h3>
                <p onClick={() => handleClick(content[3].url)}>
                  {content[3].description}
                </p>
              </div>
              {content[3].image ? (
                <img
                  onClick={() => handleClick(content[3].url)}
                  src={content[3].image}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SecondaryContent;
