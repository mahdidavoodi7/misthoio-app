import styles from "./styles.module.scss";

const Loading = ({isWhite = false}) => {
  return (
    <div class={`${styles.ldsRing} ${isWhite ? styles.white : ""}`}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Loading;
