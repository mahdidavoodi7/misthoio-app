import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Loading from "../../components/Loading/Loading";
import styles from "./styles.module.scss";

const Post = (props) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const [data, setData] = useState({});

  useEffect(() => {
    setLoading(true);
    setHasError(false);
    axios
      .get(`/api/post/${props.match.params.id}`)
      .then((res) => {
        setLoading(false);
        setHasError(false);
        setData(res.data);
      })
      .catch((err) => {
        setLoading(false);
        setHasError(true);
        if (err.response.status === 401) {
          history.push("/login");
        }
      });
  }, []);

  if (hasError && !loading) {
    return <div></div>;
  } else if (loading) {
    return(
      <div className={styles.loadingContainer}>
        <Loading />
        <p>Scrapping website, Please wait...</p>
      </div>
    );
  }
  return (
    <div className={styles.container}>
      <h1>
      {data.headline}
      </h1>
      <img src={data.image} />
      <div dangerouslySetInnerHTML={{ __html: data.body }}></div>
    </div>
  );
};
export default Post;
