import styles from "./styles.module.scss";

const TopContent = ({ content = [], handleClick }) => {
  if (content.length === 0) {
    return null;
  }
  return (
    <>
      <div className={styles.topContent}>
        <div className={styles.left}>
          <img
            onClick={() => handleClick(content[0].url)}
            src={content[0].image}
          />
          <span onClick={() => handleClick(content[0].url)}>
            {content[0].tag}
          </span>
          <h3 onClick={() => handleClick(content[0].url)}>
            {content[0].title}
          </h3>
          <p onClick={() => handleClick(content[0].url)}>
            {content[0].description}
          </p>
        </div>
        <div className={styles.right}>
          <div className={styles.rightHalf}>
            <div className={`${styles.card} ${styles.withBorder}`}>
              <img
                onClick={() => handleClick(content[1].url)}
                src={content[1].image}
              />
              <span onClick={() => handleClick(content[1].url)}>
                {content[1].tag}
              </span>
              <h3 onClick={() => handleClick(content[1].url)}>
                {content[1].title}
              </h3>
              <p onClick={() => handleClick(content[1].url)}>
                {content[1].description}
              </p>
            </div>
            <div className={styles.card}>
              <span onClick={() => handleClick(content[2].url)}>
                {content[2].tag}
              </span>
              <h3 onClick={() => handleClick(content[2].url)}>
                {content[2].title}
              </h3>
              <p onClick={() => handleClick(content[2].url)}>
                {content[2].description}
              </p>
            </div>
          </div>
          <div className={styles.theWorldInBrief}>
            <div className={styles.theWorldInBriefContent}>
              <h2>The world in brief</h2>
              <p>
                This project is designed by Mahdi Davoodi Lorem Ipsum is simply
                dummy text of the printing and typesetting industry. Lorem Ipsum
                has been the industry's standard dummy text ever since the
                1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not
                only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised
                in the 1960s with the release of Letraset sheets containing
                Lorem Ipsum passages, and more recently with desktop publishing
                software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.secondSection}>
        <div className={styles.part}>
          <div className={styles.card}>
            <img
              onClick={() => handleClick(content[3].url)}
              src={content[3].image}
            />
            <h3 onClick={() => handleClick(content[3].url)}>
              {content[3].title}
            </h3>
            <p onClick={() => handleClick(content[3].url)}>
              {content[3].description}
            </p>
          </div>
        </div>
        <div className={styles.part}>
          <div className={styles.card}>
            <img
              onClick={() => handleClick(content[4].url)}
              src={content[4].image}
            />
            <h3 onClick={() => handleClick(content[4].url)}>
              {content[4].title}
            </h3>
            <p onClick={() => handleClick(content[4].url)}>
              {content[4].description}
            </p>
          </div>
        </div>
        <div className={styles.part}>
          <div className={styles.card}>
            <img
              onClick={() => handleClick(content[5].url)}
              src={content[5].image}
            />
            <h3 onClick={() => handleClick(content[5].url)}>
              {content[5].title}
            </h3>
            <p onClick={() => handleClick(content[5].url)}>
              {content[5].description}
            </p>
          </div>
        </div>
        <div className={styles.part}>
          <div className={styles.card}>
            <img
              onClick={() => handleClick(content[6].url)}
              src={content[6].image}
            />
            <h3 onClick={() => handleClick(content[6].url)}>
              {content[6].title}
            </h3>
            <p onClick={() => handleClick(content[6].url)}>
              {content[6].description}
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default TopContent;
