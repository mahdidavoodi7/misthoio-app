import styles from "./styles.module.scss";

const SubjectContent = ({ content = {}, handleClick }) => {
  if (!content) {
    return null;
  }
  return (
    <div className={styles.subjectContent}>
      {Object.keys(content).map((key) => {
        return (
          <div className={styles.part}>
            <div className={styles.innerPart}>
              <h2>{key}</h2>
              <img
                onClick={() => handleClick(content[key][0].url)}
                src={content[key][0].image}
              />
              <h3 onClick={() => handleClick(content[key][0].url)}>
                {content[key][0].title}
              </h3>
              <h3 onClick={() => handleClick(content[key][1].url)}>
                {content[key][1].title}
              </h3>
              <h3 onClick={() => handleClick(content[key][2].url)}>
                {content[key][2].title}
              </h3>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default SubjectContent;
