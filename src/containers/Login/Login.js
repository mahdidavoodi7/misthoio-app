import axios from "axios";
import { useState } from "react";
import styles from "./styles.module.scss";
import { useHistory } from "react-router-dom";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const handleLogin = (e) => {
    e.preventDefault();
    axios
      .post("/api/auth/", {
        email,
        password,
      })
      .then((res) => {
        history.push("/")
      })
      .catch((err) => {});
  };
  return (
    <form onSubmit={handleLogin} className={styles.container}>
      <label className={styles.label} htmlFor="email">
        Email
      </label>
      <input
        onChange={(e) => setEmail(e.target.value)}
        className={styles.input}
        id="email"
        type="text"
      ></input>
      <label className={styles.label} htmlFor="password">
        Password
      </label>
      <input
        onChange={(e) => setPassword(e.target.value)}
        className={styles.input}
        id="password"
        type="password"
      ></input>
      <button type="submit" className={styles.button}>
        Login
      </button>
    </form>
  );
};
export default Login;
