import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Home from "./containers/Home/Home";
import Login from "./containers/Login/Login";
import Post from "./containers/Post/Post";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/post/:id" exact component={Post} />
        <Route path="/login" exact component={Login} />
      </Switch>
    </Router>
  );
}

export default App;
