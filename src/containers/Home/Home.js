import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Loading from "../../components/Loading/Loading";
import SecondaryContent from "../../components/SeoncadyContent/SecondaryContent";
import PandemicContent from "../../components/SeoncadyContent/SecondaryContent";
import SubjectContent from "../../components/SubjectContent/SubjectContent";
import TopContent from "../../components/Top/Top";
import styles from "./styles.module.scss";

const Home = (props) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [loadingOnePost, setLoadingOnePost] = useState(false);

  const [hasError, setHasError] = useState(false);
  const [topContent, setTopContent] = useState([]);
  const [theWorldAheadContent, setTheWorldAheadContent] = useState([]);
  const [subjectsContent, setSubjectsContent] = useState({});
  const [pandemicContent, setPandemicContent] = useState([]);

  const handleGetSinglePost = (url) => {
    setLoadingOnePost(true);
    axios
      .post("/api/post/", {
        url: url,
      })
      .then((res) => {
        setLoadingOnePost(false);
        history.push(`/post/${res.data}`);
      })
      .catch((err) => {
        setLoadingOnePost(false);
        console.log(err);
      });
  };

  useEffect(() => {
    setLoading(true);
    setHasError(false);
    axios
      .get("/api/post/")
      .then((res) => {
        setLoading(false);
        setHasError(false);
        setTopContent(res.data.topContent);
        setTheWorldAheadContent(res.data.theWorldAheadContent);
        setSubjectsContent(res.data.subjectsContent);
        setPandemicContent(res.data.pandemicContent);
      })
      .catch((err) => {
        setLoading(false);
        setHasError(true);
        if (err.response.status === 401) {
          history.push("/login");
        }
      });
  }, []);

  if (hasError && !loading) {
    return <div></div>;
  } else if (loading) {
    return (
      <div className={styles.loadingContainer}>
        <Loading />
        <p>Scrapping the website, Please wait...</p>
      </div>
    );
  }
  return (
    <div className={styles.container}>
      {loadingOnePost ? (
        <div className={styles.overlay}>
          <Loading isWhite />
          <p>Scrapping a post, Please wait...</p>
        </div>
      ) : null}
      <TopContent content={topContent} handleClick={handleGetSinglePost} />
      <SecondaryContent
        content={pandemicContent}
        title={"The pandemic’s economic effects"}
        handleClick={handleGetSinglePost}
      />
      <SecondaryContent
        content={theWorldAheadContent}
        title={"The World Ahead 2022"}
        handleClick={handleGetSinglePost}
      />
      <SubjectContent
        content={subjectsContent}
        handleClick={handleGetSinglePost}
      />
    </div>
  );
};
export default Home;
